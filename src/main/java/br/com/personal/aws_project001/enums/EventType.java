package br.com.personal.aws_project001.enums;

public enum EventType {
    PRODUCT_CREATED,
    PRODUCT_UPDATE,
    PRODUCT_DELETED
}
