package br.com.personal.aws_project001;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AwsProject001Application {

    public static void main(String[] args) {
        SpringApplication.run(AwsProject001Application.class, args);
    }

}
