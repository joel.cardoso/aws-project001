package br.com.personal.aws_project001.config.local;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.BucketNotificationConfiguration;
import com.amazonaws.services.s3.model.S3Event;
import com.amazonaws.services.s3.model.TopicConfiguration;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.CreateTopicRequest;
import com.amazonaws.services.sns.util.Topics;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("local")
public class S3ConfigLocal {

    private static final String BUCKET_NAME = "pcs-invoice";
    private AmazonS3 amazonS3;

    public S3ConfigLocal() {
        amazonS3 = getAmazonS3();

        createBucket();

        AmazonSNS amazonSNS = getAmazonSNS();

        String s3InvoiceEventsTopicArn = createTopic(amazonSNS);

        AmazonSQS amazonSQS = getAmazonSQS();

        createQueue(amazonSNS, s3InvoiceEventsTopicArn, amazonSQS);

        configureBucket(s3InvoiceEventsTopicArn);
    }

    private void configureBucket(String s3InvoiceEventsTopicArn) {
        TopicConfiguration topicConfiguration = new TopicConfiguration();
        topicConfiguration.setTopicARN(s3InvoiceEventsTopicArn);
        topicConfiguration.addEvent(S3Event.ObjectCreatedByPut);

        amazonS3.setBucketNotificationConfiguration(BUCKET_NAME, new BucketNotificationConfiguration().addConfiguration("putObject", topicConfiguration));
    }

    private void createQueue(AmazonSNS amazonSNS, String s3InvoiceEventsTopicArn, AmazonSQS amazonSQS) {
        String s3InvoiceEventsQueueUrl = amazonSQS.createQueue(new CreateQueueRequest("s3-invoice-events")).getQueueUrl();

        Topics.subscribeQueue(amazonSNS, amazonSQS, s3InvoiceEventsTopicArn, s3InvoiceEventsQueueUrl);
    }

    private String createTopic(AmazonSNS amazonSNS) {
        CreateTopicRequest createTopicRequest = new CreateTopicRequest("s3-invoice-events");
        return amazonSNS.createTopic(createTopicRequest).getTopicArn();
    }

    private AmazonS3 getAmazonS3() {
        AWSCredentials awsCredentials = new BasicAWSCredentials("test", "test");
        this.amazonS3 =  AmazonS3Client.builder()
                .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration("http://localhost:4566", Regions.US_EAST_1.getName()))
                .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
                .enablePathStyleAccess()
                .build();
        
        return this.amazonS3;
    }

    private void createBucket() {
        this.amazonS3.createBucket(BUCKET_NAME);
    }

    @Bean
    public AmazonS3 amazonS3Client() {
        return this.amazonS3;
    }

    private AmazonSNS getAmazonSNS() {
        return AmazonSNSClient.builder().withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration("http://localhost:4566", Regions.US_EAST_1.getName()))
                .withCredentials(new DefaultAWSCredentialsProviderChain())
                .build();
    }

    private AmazonSQS getAmazonSQS() {
        return AmazonSQSClient.builder().withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration("http://localhost:4566", Regions.US_EAST_1.getName()))
                .withCredentials(new DefaultAWSCredentialsProviderChain())
                .build();
    }

}
